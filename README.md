# Helpful Exceptions

A set of helpful exceptions for all your development needs.

A work in progress, so please provide feedback where possible.

The code is provided under MIT license, so it's permissive to use it for all kinds of derivative works that the license permits.

Thank you for your time and your willingness to go through this README