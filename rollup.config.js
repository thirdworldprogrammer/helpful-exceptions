import { babel } from '@rollup/plugin-babel';

export default {
  input: 'src/main.ts',
  output: {
    file: 'dist/bundle.js',
    format: 'umd',
    name: 'helpfulExceptions',
  },
  plugins: [babel({ babelHelpers: 'bundled', extensions: ['.js', '.ts'] })],
};
